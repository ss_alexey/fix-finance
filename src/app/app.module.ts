import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './pages/main/main.component';
import { DailyAnaliticsComponent } from './components/daily-analitics/daily-analitics.component';
import { StartTraidingComponent } from './components/start-traiding/start-traiding.component';
import { MarketComponent } from './components/market/market.component';
import { ContactComponent } from './components/contact/contact.component';
import { MeetingComponent } from './components/meeting/meeting.component';

import { SharedModule } from './shared/shared.module';
import { NavigateSliderComponent } from './components/navigate-slider/navigate-slider.component';
import { PaymentSliderComponent } from './components/payment-slider/payment-slider.component';





@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    DailyAnaliticsComponent,
    StartTraidingComponent,
    MarketComponent,
    ContactComponent,
    MeetingComponent,
    NavigateSliderComponent,
    PaymentSliderComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    SharedModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
