import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StartTraidingComponent } from './start-traiding.component';

describe('StartTraidingComponent', () => {
  let component: StartTraidingComponent;
  let fixture: ComponentFixture<StartTraidingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartTraidingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartTraidingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
