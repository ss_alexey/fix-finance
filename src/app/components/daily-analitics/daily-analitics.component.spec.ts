import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyAnaliticsComponent } from './daily-analitics.component';

describe('DailyAnaliticsComponent', () => {
  let component: DailyAnaliticsComponent;
  let fixture: ComponentFixture<DailyAnaliticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyAnaliticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyAnaliticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
