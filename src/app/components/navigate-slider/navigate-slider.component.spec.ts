import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigateSliderComponent } from './navigate-slider.component';

describe('NavigateSliderComponent', () => {
  let component: NavigateSliderComponent;
  let fixture: ComponentFixture<NavigateSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigateSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigateSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
