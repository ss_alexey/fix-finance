import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';

import { MatIconModule } from '@angular/material/icon';
import {AngularModule} from './angular/angular.module';
import {MaterialModule} from './material/material.module';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent],
  imports: [
    MaterialModule,
    AngularModule,
    CommonModule,
    MatIconModule
  ],
  exports: [
    MaterialModule,
    AngularModule,
    MatIconModule,
    HeaderComponent, FooterComponent
  ]
})
export class SharedModule { }
