import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatButtonModule, MatCheckboxModule } from '@angular/material';

const modules = [MatButtonModule, MatCheckboxModule];

@NgModule({
  declarations: [],
  imports: [
    modules,
    CommonModule
  ],
  exports: [modules]
})
export class MaterialModule { }
